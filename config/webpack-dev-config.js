var path    = require("path");
var webpack = require("webpack");

var appPath= path.resolve('..', 'src')

module.exports = {
    devtool: "eval"
    /*
  , entry : {
        "main" : [
            path.resolve(appPath , 'app', 'app.js')
        ]
    }
    */
  , output : {
        path : path.resolve('..', 'build')
    }
  , resolve : {
        extensions : ['', '.js', '.jsx'] // allow require('./foo') to be these extensions
    }
  , plugins: [

        // print a webpack progress
        new webpack.ProgressPlugin( function(percentage, message) {
            var MOVE_LEFT = new Buffer("1b5b3130303044", "hex").toString();
            var CLEAR_LINE = new Buffer("1b5b304b", "hex").toString();
            process.stdout.write(CLEAR_LINE + Math.round(percentage * 100) + '%:' + message + MOVE_LEFT);
        }),

        new webpack.DefinePlugin({
            "process.env": {
                BROWSER: JSON.stringify(true),
                NODE_ENV: JSON.stringify("development")
            }
        }),

        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
    ]
}

