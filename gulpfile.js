var fs   = require('fs');
var path = require('path');
var help = require('gulp-help');
var gulp = help(require('gulp'));

var taskPath = './task'

// Load all task files
fs.readdirSync(taskPath).forEach(function(fileName){
    var filePath = path.resolve(taskPath, fileName);
    if (fs.lstatSync(filePath).isFile()) {
        require(filePath);
    }
})

// default it to help
gulp.task('default', 'List all gulp tasks in /task/', ['help'])

