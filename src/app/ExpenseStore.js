import Http           from '../core/lib/Http'
import {loginSession} from './util/CacheService'

var HttpParams = {
    headers : {
        'Authorization' : loginSession.get('token')
    }
}

export default class ExpenseStore {
    constructor() {
        this.data
    }
    getExpenses() {
        return this.data = Http.get('http://localhost:3334/v1/expenses', HttpParams)
    }
}
