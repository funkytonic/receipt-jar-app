import Defer       from 'promise-defer'
import Http        from '../core/lib/Http'
import rx          from 'rx'
import {dataCache} from './util/CacheService'

var subject = new rx.Subject()

export default class PostStore {
    constructor() {
        this.data
    }
    get() {
        var self = this
        var defer = Defer()

        var data = this.data = dataCache.get('postdata')

        if (data) {
            defer.resolve(data)
        } else {
            Http.get('http://jsonplaceholder.typicode.com/posts').then(function(resp){
                var data = JSON.parse(resp.data)
                dataCache.set('postdata', data)
                defer.resolve(data)
            })
        }

        return defer.promise
    }
    create() {

    }
    subscribe(cb) {
        return subject.subscribe(cb)
    }
}

