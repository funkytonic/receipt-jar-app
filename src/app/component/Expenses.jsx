import { fromJS, List, Map }  from 'immutable'
import React  from 'react'
import {ajax} from 'nanoajax'
import Store from '../ExpenseStore'

var ExpenseStore = new Store()
const PureRenderMixin = require('react/addons').addons.PureRenderMixin

export default class Table extends React.Component {
    constructor() {
        super()
        this.state = {
            data         : List()
          , filteredData : List()
        }
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    }

    componentDidMount() {
        var self = this

        ExpenseStore.getExpenses().then(function(payload){
            self.setState({
                data : JSON.parse(payload.data)
            })
        })
        .catch(function(err){
            console.log(err)
        })

    }

    render() {
        var data = this.state.data.map(function(item){
            return(
                <tr>
                    <td>{item.title}</td>
                    <td>{item.amo}</td>
                    <td>{item.type}</td>
                    <td>{item.loc}</td>
                    <td>[...]</td>
                </tr>
            )
        })
        return (
            <div className="table-container">
                <table>
                    <thead>
                        <th>Title</th>
                        <th>Amount</th>
                        <th>Type</th>
                        <th>Location</th>
                    </thead>
                    <tbody>
                        {data}
                    </tbody>
                </table>
            </div>
        )
    }
}

