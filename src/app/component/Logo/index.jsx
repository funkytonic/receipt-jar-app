import React from 'react'

export default class Logo extends React.Component {
    render() {
        var image = require('file!./logo.png')
        
        return (
            <div className="logo">
                <img src={image} alt="" />
            </div>
        )
    }
}

