import React from 'react'
import decode from 'jwt-decode'
import {loginSession} from '../util/CacheService'

export default class PageAbout extends React.Component {
    constructor() {
        super()
    }
    logout() {
        loginSession.delete('token')
        window.location.href = '/'
    }
    render() {
        var user
        var isLoggedin = loginSession.get('token')
        if (isLoggedin) {
            user = decode(isLoggedin)
            name = user.name
        }
        return(
            <div className="fright">
                {{name}}  <a href="#" onClick={this.logout}>logout</a>
            </div>
        )
    }
}

