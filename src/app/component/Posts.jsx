import { fromJS, List, Map }  from 'immutable'
import React  from 'react'
import {ajax} from 'nanoajax'
import PostStore from '../PostStore'

var Posts = new PostStore()

const PureRenderMixin = require('react/addons').addons.PureRenderMixin

export default class Table extends React.Component {
    constructor() {
        super()
        this.state = {
            data         : List()
          , filteredData : List()
        }
        this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
    }

    componentDidMount() {
        var self = this

        var PostsData = Posts.get().then(function(data){
            self.setState({
                  data         : fromJS(data).toList()
                , filteredData : fromJS(data).toList()
            })
        })

    }

    filterData(evt) {
        evt.preventDefault()
        const regex = new RegExp(evt.target.value, 'i')
        const filtered = this.state.data.filter(datum => {
            return (datum.get('title').search(regex) > -1)
        })

        this.setState({
            filteredData : filtered
        })
    }

    render() {
        const {filteredData} = this.state
        const prettyRows = filteredData.map(function(datum){
            return (
                <tr>
                    <td>{ datum.get("id") }</td>
                    <td>{ datum.get("title") }</td>
                </tr>
            )
        })

        return (
            <div className="table-container">
                <input type="text" onChange={this.filterData.bind(this)} />

                <table>
                    <thead>
                        <th>ID</th>
                        <th>Title</th>
                    </thead>
                    <tbody>
                        {prettyRows}
                    </tbody>
                </table>
            </div>
        )
    }
}

