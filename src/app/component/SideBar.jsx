import React from 'react'
import {Link, RouteHandler} from 'react-router'

export default class SideBar extends React.Component {
    displayName : "SideBar"
    render() {
        return(
            <div id="site-sidebar">
                <ul>
                    <li><Link to="/">Expense</Link></li>
                    <li><Link to="/summary">Summary</Link></li>
                    <li><Link to="/tools">Tools</Link></li>
                    <li><Link to="/settings">Settings</Link></li>
                    <li><Link to="/about">News</Link></li>
                </ul>
            </div>
        )
    }
}
