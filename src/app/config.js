export default {
    loginSessionTimeout : 1000 * 60 * 29 // 1 hour
  , requestTimeout      : 1000 * 60 * 3 // 3min
}
