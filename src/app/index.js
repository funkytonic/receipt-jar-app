import React  from 'react'
import Router from 'react-router'

var {
    Route,
    State,
    NotFoundRoute,
    DefaultRoute,
    RouteHandler,
    HistoryLocation,
    run
} = Router

var routes = (
    <Route>
        <Route path="/" handler={require('./view/Layout')}>
            // TODO: make sure expense route instead of home route
            <DefaultRoute          handler ={require('./view/PageHome')}/>
            <Route path="about"    handler ={require('./view/PageAbout')}/>
            <Route path="tools"    handler ={require('./view/PageTools')}/>
            <Route path="summary"  handler ={require('./view/PageSummary')}/>
            <Route path="settings" handler ={require('./view/PageSettings')}/>
        </Route>
        <Route path="login"    handler ={require('./view/PageLogin')}/>
    </Route>
)

run(routes, HistoryLocation, ( Handler ) => {
    React.render(<Handler />, document.body)
})

