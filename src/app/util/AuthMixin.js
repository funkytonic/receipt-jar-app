export default {
    checkAuth(transition) {
        var isLoggedin = sessionStorage.getItem('user')
        if (!isLoggedin) {
            transition.redirect("/login");
        }
    }
}

