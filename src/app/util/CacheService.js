import Cache from '../../core/lib/Cache'

import {
    loginSessionTimeout
} from '../config'

export default {
    loginSession : new Cache('sessionStorage', loginSessionTimeout)
  , dataCache    : new Cache('localStorage', false)
}

