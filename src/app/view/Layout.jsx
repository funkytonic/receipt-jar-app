import React from 'react'
import {Link, RouteHandler} from 'react-router'

// components
import SideBar    from '../component/SideBar'
import Logo       from '../component/Logo/index'
import MenuAvatar from '../component/MenuAvatar'

class AppHeader extends React.Component {
    displayName : "AppLayout"
    render() {
        return(
            <div id="site-header">
                <div className="header-sidebar-col">
                    <Logo />
                </div>
                <input className="search" type="text" placeholder="Help..." />
                <MenuAvatar id="profile-status" />
            </div>
        )
    }
}
class AppFooter extends React.Component {
    render() {
        return(
            <div id="site-footer">Footer</div>
        )
    }
}

export default class AppLayout extends React.Component {
    constructor() {
        super()
    }

    render() {

        var loggedinUser = function() {
            return (
                <div>
                    <AppHeader/>
                    <div id="site-main">
                        <SideBar />
                        <div id="site-content">
                            <RouteHandler />
                        </div>
                    </div>
                    <AppFooter/>
                </div>
            )
        }

        return(
            loggedinUser()
        )
    }
}

