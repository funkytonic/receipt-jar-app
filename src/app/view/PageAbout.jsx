import React from 'react'
import Posts from '../component/Posts'
import {loginSession} from '../util/CacheService'

export default class PageAbout extends React.Component {
    static willTransitionTo(transition) {
        var isLoggedin = loginSession.get('token')
        if (!isLoggedin) {
            transition.redirect("/login");
        }
    }

    constructor() {
        super()
    }

    render() {
        return (
            <div>
                <h2>News</h2>
                <Posts/>
            </div>
        )
    }
}

