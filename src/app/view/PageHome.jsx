import React          from 'react'
import Expenses       from '../component/Expenses'
import {loginSession} from '../util/CacheService'

// TODO: change the name, it should be expenses
export default class PageHome extends React.Component {
    static willTransitionTo(transition) {
        var isLoggedin = loginSession.get('token')
        if (!isLoggedin) {
            transition.redirect("/login");
        }
    }

    render() {
        return (
            <div>
                <h3>Expenses</h3>
                <Expenses />
            </div>
        )
    }
}

