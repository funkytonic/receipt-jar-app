import React          from 'react'
import router         from 'react-router'
import mixin          from 'react-mixin'
import AuthService    from '../../core/lib/AuthService'
import {loginSession} from '../util/CacheService'

// TODO: change the name, it should be expenses
export default class PageLogin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            msg  : ''
        }
    }
    login(e) {
        var _this = this
        e.preventDefault()

        var user= this.refs.userInput.getDOMNode().value
          , pass= this.refs.passInput.getDOMNode().value

        if (user !== '' && pass !== '') 
        {
            AuthService.login().then(function(resp){
                loginSession.set('token', resp.data)
                _this.transitionTo('/')
            })
        }
        else
        {
            this.setState({msg:'User name Password incorect'})
        }
    }
    render() {
        return (
            <form onSubmit={this.login.bind(this)}>
                <input type="text"     placeholder="Username" ref='userInput' />
                <input type="password" placeholder="Password" ref='passInput' />
                <input type="submit" value="Sign In" />
                <hr />
                {this.state.msg}
            </form>
        )
    }
}

mixin.onClass(PageLogin, router.Navigation)

