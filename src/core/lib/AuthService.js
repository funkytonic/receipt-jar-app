import {ajax} from 'nanoajax'
import Defer  from 'promise-defer'
import Http from './Http'

var url = "http://localhost:3334/v1/login"

var fakeUser = JSON.stringify({
    "username" : "test"
  , "password" : "pass"
})

export default {
    login() {
        return Http.post(url, fakeUser)
    }

  , logout() {
        sessionStorage.removeItem('token')
    }
}

