import rx from 'rx'

class _wrappedAction {
    constructor() {
        var stream = new rx.Subject()
        
        // wrapper
        var action = function action(payload) {
            stream.onNext(payload)
        }

        // clone method
        for (let prop in stream) {
            action[prop] = stream[prop]
        }

        // map complete to done
        action.done = stream.onCompleted.bind(action)

        // return the wrapper
        return action
    }
}

class Action {
    constructor(list) {
        var actions = {}
        for (let action of list) {
            actions[action] = new _wrappedAction()
        }

        return actions
    }
}

class Store {
    constructor(opts) {
        var thisStore = this

        // create an update event
        this.update = rx.Subject()
       
        // flat the array of objects
        var actions = {}
        if (opts && opts.listenTo) {
            actions = opts.listenTo.reduce(a,b => {
                for (let i in b) {
                    a[i] = b[i]
                }
            })
        }

        // subscribe actions
        for (let i in actions) {
            actions[i].subscribe(thisStore['on'+i].call(thisStore, data))
        }
    }
    subscribe(cb) {
        this.update.subscribe(cb)
    }
}

export {Action, Store}

