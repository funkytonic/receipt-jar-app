import Defer  from 'promise-defer'

export default {
    request
  , get(url, params) {
        var params     = params || {}
            params.url = url
        return request(params)
    }
  , post(url, data, params) {
        var params         = params || {}
            params.url     = url
            params.method  = 'POST'
            params.body    = data
            params.headers = params.headers || { 'Content-Type' : 'application/json' }

        return request(params)
    }
  , put(url, data, params) {
        var params         = params || {}
            params.url     = url
            params.method  = 'PUT'
            params.body    = data
            params.headers =  params.headers || {'Content-Type': 'application/json'}

        return request(params)
    }
  , delete(url) {
        var params        = params || {}
            params.url    = url
            params.method = 'DELETE'

        return request(params)
    }
}

/**
 *
 * @param {Object}             params.headers
 * @param {String}             params.method
 * @param {String}             params.url
 * @param {String} [Optional]  params.body
 */
function request(params) {
    var defer   = Defer()
      , headers = params.headers || {}
      , timeout = params.timeout || 30000 // 30 seconds

      , method  = params.method || 'GET'
      , req     = new window.XMLHttpRequest()
      , code    = req.status

    // default headers
    if (!headers['X-Requested-With']) headers['X-Requested-With'] = 'XMLHttpRequest';
    if (!headers['Content-Type'])     headers['Content-Type']     = 'application/x-www-form-urlencoded';

    req.onreadystatechange = function() {
        // when response ready
        if ( req.readyState!=4 ) return;

        if ( 200 <= code && code < 300 ) {
            defer.reject({status:req.status, data:req.responseText, res:req.response})
        } else {
            defer.resolve({status:req.status, data:req.responseText, res:req.response})
        }
    }
    req.timeout = timeout
    req.ontimeout = function() { defer.reject({status:req.status, data:req.responseText, res:response}) }

    req.open(method, params.url, true)

    // all headers
    for (let field in headers) {
        req.setRequestHeader(field, headers[field])
    }

    req.send(params.body)

    return defer.promise
}

