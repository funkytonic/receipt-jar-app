export default class Cache {
    // in 60min
    // ms   * s  * m
    // 1000 * 60 * 60
    constructor(mode='cache',expires=3600000) {
        this.engine
        this.mode = mode
        this.expires = expires

        if (mode==='localStorage') {
            this.engine = window.localStorage ? window.localStorage : null
        }

        if (mode==='sessionStorage') {
            this.engine = window.sessionStorage ? window.sessionStorage : null
        }

        if (!this.engine || mode==='cache') {
            this.engine = {}
        }
    }

    get(key) {
        var payload = (this.mode==='cache') ? this.engine[key] : JSON.parse(this.engine.getItem(key))

        if (payload) {
            if (!payload.content || payload.expires && +new Date(payload.expires) < +new Date()) {
                this.delete(key)
                return null
            }
            return payload.content
        }
        return null
    }

    set(key, val, expires) {
        var _this = this
        var itExpires = _this.expires ? +new Date() + ( expires ? expires : _this.expires ) : false

        var payload = {
            content : val
          , expires : itExpires
        }

        if (this.mode==='cache') {
            this.engine[key] = payload
            return
        }
        this.engine.setItem(key, JSON.stringify(payload))
    }

    delete(key) {
        if (this.mode==='cache'){
            delete this.engine[key]
            return
        }
        this.engine.removeItem(key);
    }

    flush() {
        if (this.mode==='cache'){
            this.engine = {}
            return
        }
        this.engine.clear();
    }
}

