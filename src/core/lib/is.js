/**
 * Type checker
 */

/**
 * Object.prototype.toString.call(yourObj) gives
 * '[object Null]',
 * '[object Window]',
 * '[object Array]',
 * '[object Object]',
 * '[object HTMLDocument]',
 * '[object Function]'
 */

/**
 * Get type
 * @param {Object} thing
 * @return {String} eg: String, Object, Array, Null, Window, HTMLDocumnet
 */
function getType(thing){
    if(thing === null) return 'Null'; // special case
    return Object.prototype.toString.call(thing).slice(8, -1);
}

/**
 * is checker
 * @param {Object} obj any thing you want to check
 * @param {String} type the type you want to compare
 * @return {Boolean} true false
 */
function isType(obj, type) {
    return obj !== undefined && obj !== null && getType(obj).toLowerCase() === type.toLowerCase(); 
}

/**
 * check if object is null
 *
 * @param obj Object
 * @returns {boolean}
 */
function isNull(obj) {
    return obj === null;
}

/**
 * is checker, wrapped for shortcut
 * @return {Function}
 */
function typeCheck(type) {
    return function(obj){
        return isType(obj, type);
    };
}

export default {
    getType: getType,
    is: isType,
    'string': typeCheck('string'),
    'object': typeCheck('object'),
    'number': typeCheck('number'),
    'array': typeCheck('array'),
    'null': isNull,
    'function': typeCheck('function'),
    'date': typeCheck('date')
}

