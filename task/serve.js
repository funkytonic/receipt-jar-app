var gulp        = require('gulp-help')(require('gulp'));
var plumber     = require('gulp-plumber');
var webpack     = require('gulp-webpack')
var path        = require('path')
var browserSync = require('browser-sync')
var reload      = browserSync.reload
var modRewrite  = require('connect-modrewrite')
var _           = require('lodash')
var stylus      = require('gulp-stylus')
var sourcemap   = require('gulp-sourcemaps')



var confs = {
    devtool : "#source-map"
  , watch   : true
  , output  : {
        filename : 'bundle.js'
    }
  , module : {
        loaders : [
            { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader'}
          , { test: /\.jsx$/, exclude: /node_modules/, loader: 'babel-loader'}
        ]
    }
}

var newConfs = _.merge(confs, require('../config/webpack-dev-config'))

var appPath = path.resolve(__dirname, '..')

gulp.task('build:css', '- Build css', function() {
    gulp.src(appPath+'/src/asset/css/style.styl')
        .pipe(plumber())
        .pipe(sourcemap.init())
        .pipe(stylus({
            'include css' : true
          , 'lineno'      : true
        }))
        .pipe(sourcemap.write())
        .pipe(gulp.dest(appPath+'/build'))
})

gulp.task('serve', '= Serve app', function() {
    browserSync.init({
        server : {
            baseDir : [appPath + '/src/', appPath + '/build/' ]
          , middleware : [
                modRewrite(['!\\.\\w+$ /index.html [L]'])
            ]
        }
    })

    gulp.watch(appPath + '/build/**/*', reload)
    gulp.watch(appPath + '/src/**/*.html', reload)
    gulp.watch(appPath + '/src/asset/css/style.styl', ['build:css'])

    return gulp.src(appPath + '/src/app/index.js')
               .pipe(plumber())
               .pipe(webpack(confs))
               .pipe(plumber.stop())
               .pipe(gulp.dest(appPath + '/build'))
})

